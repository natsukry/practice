package com.natsu.service.impl;

import com.natsu.dao.UserDao;
import com.natsu.entity.User;
import com.natsu.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    public List<User> listUsers() {
        return userDao.list();
    }

    @Override
    public void saveOne(User user) {
        userDao.save(user);
    }
}
