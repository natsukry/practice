package com.natsu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.natsu.entity.User;

import java.util.List;

public interface UserService {
    List<User> listUsers();

    void saveOne(User user);
}
