package com.natsu.controller;

import com.natsu.entity.User;
import com.natsu.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class AppController {

    private final UserService userService;

    public AppController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> list(){
        return ResponseEntity.ok(userService.listUsers());
    }

    @PostMapping("/user")
    public ResponseEntity saveOne(@RequestBody User user){
        userService.saveOne(user);
        return ResponseEntity.accepted().build();
    }
}
