package com.natsu.dao;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.natsu.entity.User;
import com.natsu.mapper.UserMapper;
import org.springframework.stereotype.Service;

@Service
public class UserDao extends ServiceImpl<UserMapper, User> {
}
